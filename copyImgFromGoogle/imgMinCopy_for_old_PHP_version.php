<? header('Content-Type: text/html; charset=utf-8');
/*
скрипт для оптимизации изображений, полученных с сайта https://developers.google.com/speed/pagespeed/insights/
для начала работы необходимо дать файлу скрипта права 0755. Потом делаем обратно 0644 !!!
Делаем анализ через https://developers.google.com/speed/pagespeed/insights/
Затем открываем консоль, блоку с урлами добавляем класс "my". Потом в консоли вводим 4 строки, потом Enter:

let urls=document.querySelectorAll('.url-block.my [data-title]');let arr = []; let result = [];
for(let i=0;i<urls.length;i++){arr.push(urls[i].dataset.title);}
for(let i in arr){let ddd=(arr[i].split('/')).slice(3);result.push(ddd.join('/'));}
console.log(result);  // ---> в итоге мы получаем массив из нужных ссылок на изображения
*/

$imgMinFolder = '_image_min/'; // название папки на сервере, куда мы скопируем оптимизированные изображения

$backupFolder = 'backup/'.date("H-i-s_d-m-Y"); $imgArr=array(); $i=0;

$imgPath = array(); // сюда вставляем массив ссылок, который мы получили в консоли браузера

foreach(glob($imgMinFolder.'*.*') as $filename) {
	$name = strstr($filename, '/');
	foreach($imgPath as $path) {
		if(strpos(mb_strtolower($path), $name)) {$arr = (substr($name,1), $path);}
	}
	array_push($imgArr, $arr);
}

mkdir($imgMinFolder.$backupFolder, 0755, true);
foreach($imgPath as $img) {
	$nameBackup = strrchr($img,'/');
	if(copy($img, $imgMinFolder.$backupFolder.$nameBackup)) {$backupResult = true;}
}

//echo "<pre>";
//print_r($imgArr);

echo "<table style='border-collapse:collapse;border-spacing:2px;'><style>td{border:1px solid;padding:2px 10px}</style>";
foreach($imgArr as $keys) {
	$file = $imgMinFolder.$keys[0];
	$newPath = $keys[1]; $i++;
	if(copy($file, $newPath)) {
		echo "<tr><td style='text-align:right'>{$i}</td><td>Файл {$file}</td><td><b style='color:#0bef0b'>успешно</b></td><td>скопирован по пути: /{$newPath}</td></tr>"; unlink($file);
	}else{
		echo "<tr><td style='text-align:right'>{$i}</td><td>Файл {$file}</td><td><b style='color:red'>не удалось скопировать</b></td><td>по пути /{$newPath}</td></tr>";
	}
}
echo "</table>";

if($backupResult) echo "<h3>Создана резервная копия заменяемых изображений по адресу: {$imgMinFolder}{$backupFolder}</h3>";